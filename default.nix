{ nixpkgs ? <nixpkgs> }:

with import nixpkgs {};

let
    example_environment_gcc6 = callPackage ./make_user_environment.nix {
        inherit nixpkgs;
        name = "analysis_environment_gcc6";
        extraOverlayPath = "gcc-6.nix";
    };
    example_environment_gcc7 = callPackage ./make_user_environment.nix {
        inherit nixpkgs;
        name = "analysis_environment_gcc7";
        extraOverlayPath = "gcc-7.nix";
    };
    gaudi_environment_gcc6 = callPackage ./make_user_environment.nix {
        inherit nixpkgs;
        name = "gaudi_environment_gcc6";
        extraOverlayPath = "gcc-6.nix";
        extra_packages = [ "gaudi" ];
    };
    lhcb_environment_gcc6 = callPackage ./make_user_environment.nix {
        inherit nixpkgs;
        name = "lhcb_environment_gcc6";
        extraOverlayPath = "gcc-6.nix";
        extra_packages = [ "gaudi" "lhcb" "lbcom" ];
    };
    gaudi_environment_gcc7 = callPackage ./make_user_environment.nix {
        inherit nixpkgs;
        name = "gaudi_environment_gcc7";
        extraOverlayPath = "gcc-7.nix";
        extra_packages = [ "gaudi" ];
    };
    lhcb_environment_gcc7 = callPackage ./make_user_environment.nix {
        inherit nixpkgs;
        name = "lhcb_environment_gcc7";
        extraOverlayPath = "gcc-7.nix";
        extra_packages = [ "gaudi" "lhcb" "lbcom" ];
    };
in let
    jobs = {
        # Some example environments for now
        example_environment_gcc6 = example_environment_gcc6;
        example_environment_gcc7 = example_environment_gcc7;
        gaudi_environment_gcc6 = gaudi_environment_gcc6;
        lhcb_environment_gcc6 = lhcb_environment_gcc6;
        gaudi_environment_gcc7 = gaudi_environment_gcc7;
        lhcb_environment_gcc7 = lhcb_environment_gcc7;

        # Make a replacement nixpkgs channel
        nixpkgs = pkgs.releaseTools.channel {
            constituents = [
                pkgs.gaudi
                pkgs.lhcb
                pkgs.lbcom
            ];
            name = "nixpkgs";
            src = pkgs.path;
            isNixOS = false;
        };
        # Make a channel for these environments
        lhcb_environments = pkgs.releaseTools.channel {
            constituents = [
                example_environment_gcc6
                example_environment_gcc7
                gaudi_environment_gcc6
                # lhcb_environment_gcc6
                gaudi_environment_gcc7
                # lhcb_environment_gcc7
            ];
            name = "lhcb_environments";
            src = ./.;
            isNixOS = false;
        };
    };
in jobs
